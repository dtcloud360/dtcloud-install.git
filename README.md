##  欢迎使用 DTCloud2.0
`DTCLOUD`以`BIM+AIOT`技术架构为基础，`BIM`是建筑信息模型，是空间数字化的技术；`AIOT`是智能物联网技术，将物联网系统与智能数据分析融合，提供物联采集和智能分析服务；以`BIM`空间数字化数据和`AIOT`物联终端实时采集数据为数据源，生成数字孪生模型，对工程建设和运维、企业运行和管理，提供实时智能决策。

> [DTCloud最新版本](https://gitee.com/dtcloud360/dtcloud)：https://gitee.com/dtcloud360/dtcloud

##  一键安装 DTCloud2.0
基于快速安装使用`DTCloud2.0`，我们编写了[一键安装脚本](https://gitee.com/dtcloud360/dtcloud-install)，可点击进入码云手动下载

也可利用`git`克隆:  
>  git clone  https://gitee.com/dtcloud360/dtcloud-install.git

## 使用环境 
>  推荐：#30-Ubuntu SMP Mon Apr 20 16:58:30 UTC 2020

目前Ubuntu各版本均可以。其它环境一键安装脚本正在完善中。

##  使用方法
####   1. 下载 一键安装脚本
```
git clone https://gitee.com/dtcloud360/dtcloud-install.git
```

####  2. 执行脚本
```shell
zyf@zyf-install:/opt/DTCloud_Install$ ls
dtcloud2.0.sh
zyf@zyf-install:/opt/DTCloud_Install$ 
zyf@zyf-install:/opt/DTCloud_Install$ 
zyf@zyf-install:/opt/DTCloud_Install$ 
zyf@zyf-install:/opt/DTCloud_Install$ sudo bash dtcloud2.0.sh 
中亿丰数字科技集团:一键安装dtcloud2.0
请输入服务器IP: 192.168.xxx.xxx
```
> 注意： 请用 bash 执行，这里不支持shell

#### 3. 项目配置信息
```shell
root@zyf-virtual-machine:/opt/DTCloud_Install# bash dtcloud2.0.sh 
中亿丰数字科技集团:一键安装dtcloud2.0
请输入服务器IP:192.168.32.226
请输入数据库名称:database_name
请输入数据库密码:database_password
请输入dtcloud服务端口号:9099
请输入dtcloud超级管理员密码:admin_password
请输入安装目录名: dtlcoud2
```
根据项目情况，依次填入配置信息，等待5-10分钟，即可安装完成。浏览器网页端输入url地址，即可访问并使用 `dtcloud2.0` 。

> 注意： url地址，即输入的ip:端口号。在安装完成时，命令行也会给出提示

##  服务说明 
####  1. 数据库安装在哪里？

当前方法中，代码和数据库安装在同一台服务器中。如果想分离的话，请修改脚本中数据库相关代码。

```shell
# 1.0 安装postgres 
apt install postgresql
service postgresql start
sudo su - postgres -c "createuser -s dtcloud"
sudo -u postgres psql -c "alter role dtcloud with password '$db_password'"
# 1.1 替换两个关键性配置文件
sed -i "s/^listen_addresses =.*/listen_addresses = \'\*\'/" /etc/postgresql/12/main/postgresql.conf 
sed -i "s/^#listen_addresses =.*/listen_addresses = \'\*\'/" /etc/postgresql/12/main/postgresql.conf 

function get_line(){
	sed -n -e '/IPv4 local connections/=' /etc/postgresql/12/main/pg_hba.conf 
}
line_num=$(get_line)
line_num_real=$[ `expr $line_num + 1` ]
sed -i "${line_num_real}s/127.0.0.1\/32/0.0.0.0\/0/" /etc/postgresql/12/main/pg_hba.conf 
# 1.2 重启
service postgresql restart
```

####  2. dtcloud 项目 python 依赖包如何处理的？
采用 `virtualenv` 管理 python依赖包，在项目根目录的 `env` 文件夹中

####  3. 如何关闭、打开、查看项目状态？
采用 `systemctl` 管理，如输入的安装目录名为：dtcloud2, 则相关命令为：
```
systemctl stop dtcloud2
systemctl start dtcloud2
systemctl status dtcloud2 
```
其它具体命令请参照：`man systemctl` 提示进行。
