# !/bin/bash
echo "中亿丰数字科技集团:一键安装dtcloud2.0"

echo -n "请输入服务器IP:"
read server_ip 
echo -n "请输入数据库名称:"
read db_name 
echo -n "请输入数据库密码:"
read db_password 
echo -n "请输入dtcloud服务端口号:"
read xmlrpc_port 
echo -n "请输入dtcloud超级管理员密码:"
read admin_passwd 
echo -n "请输入安装目录名:"
read dtcloud_dir

# 准备工作
apt -y --force-yes update && apt -y --force-yes upgrade # 太大了，这个更新估计要很长时间

# 1.0 安装postgres 
apt install postgresql
service postgresql start
sudo su - postgres -c "createuser -s dtcloud"
sudo -u postgres psql -c "alter role dtcloud with password '$db_password'"
# 1.1 替换两个关键性配置文件
sed -i "s/^listen_addresses =.*/listen_addresses = \'\*\'/" /etc/postgresql/12/main/postgresql.conf 
sed -i "s/^#listen_addresses =.*/listen_addresses = \'\*\'/" /etc/postgresql/12/main/postgresql.conf 

function get_line(){
	sed -n -e '/IPv4 local connections/=' /etc/postgresql/12/main/pg_hba.conf 
}
line_num=$(get_line)
line_num_real=$[ `expr $line_num + 1` ]
sed -i "${line_num_real}s/127.0.0.1\/32/0.0.0.0\/0/" /etc/postgresql/12/main/pg_hba.conf 
# 1.2 重启
service postgresql restart

# 2.0 创建用户和用户组
useradd -m -d /opt/dtcloud -U -r -s /bin/bash dtcloud

# su dtcloud
sudo apt-get install git
git clone -b DTCloud-2.0 https://gitee.com/dtcloud360/dtcloud.git /opt/dtcloud/$dtcloud_dir

# 更新社区模块
cd /opt/dtcloud/$dtcloud_dir/appstore && rm -rf dt_open_source_community 
git clone -b ubuntu https://gitee.com/dtcloud360/dt_open_source_community.git /opt/dtcloud/$dtcloud_dir/appstore/dt_open_source_community
chmod -R 755 /opt/dtcloud/$dtcloud_dir/appstore/dt_open_source_community

apt-get install build-essential
apt-get install python3.8-dev
apt-get install libpq-dev
apt-get install libxml2-dev libxslt-dev	
apt-get install libldap2-dev
apt-get install libsasl2-dev
apt-get install libzip-dev 
apt-get install node-less 

apt install python3-virtualenv
virtualenv /opt/dtcloud/$dtcloud_dir/env
/opt/dtcloud/$dtcloud_dir/env/bin/pip install -r /opt/dtcloud/$dtcloud_dir/requirements_ubuntu.txt -i https://pypi.doubanio.com/simple/

# 3.0 关于用户权限
sudo chown -R dtcloud /opt/dtcloud/$dtcloud_dir


if [ ! -d "/opt/dtcloud/$dtcloud_dir/data_dir" ]; then
	mkdir -p /opt/dtcloud/$dtcloud_dir/data_dir 
fi
sudo chown -R dtcloud /opt/dtcloud/$dtcloud_dir/data_dir

# 4.0 修改配置文件
sed -i "s/^admin_passwd =.*/admin_passwd = $admin_passwd/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^db_host =.*/db_host = $server_ip/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^db_password =.*/db_password = $db_password/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^xmlrpc_port =.*/xmlrpc_port = $xmlrpc_port/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf

sed -i "s/^db_name.*/db_name = $db_name/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^# db_name.*/db_name = $db_name/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf

sed -i "s/^data_dir =.*/data_dir = \/opt\/dtcloud\/$dtcloud_dir\/data_dir/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^local_address_go =.*/local_address_go = $server_ip:9090/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^local_upload_go =.*/local_upload_go = $server_ip:9097/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^go_api_url =.*/go_api_url = $server_ip:8088/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^venv =.*/venv = \/opt\/dtcloud\/$dtcloud_dir\/env\/bin\/python3/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf
sed -i "s/^logfile =.*/logfile = \/var\/log\/dtcloud\/$dtcloud_dir\/$dtcloud_dir.log/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf


NAME=`cat /opt/dtcloud/$dtcloud_dir/dtcloud.conf | grep 'addons_path' | awk -F '=' '{print $2}'`
arr1=()
array=(${NAME//,/ })
for var in ${array[@]}
do
	        arr1[${#arr1[@]}]=/opt/dtcloud/$dtcloud_dir/$var
done
var_addons_path=$(IFS=,; echo "${arr1[*]}")
echo $var_addons_path

var8=${var_addons_path//\//\\\/}
sed -i "s/^addons_path.*/addons_path = $var8/" /opt/dtcloud/$dtcloud_dir/dtcloud.conf


# 5.0 创建自动化服务
if [ -f "/etc/systemd/system/$dtcloud_dir.service" ] 
then
	echo "服务文件是存在的,重新覆盖内容"
else
	echo "服务文件不存在，现在创建"
	touch /etc/systemd/system/$dtcloud_dir.service
fi

cat <<EOF > /etc/systemd/system/$dtcloud_dir.service
[Unit]
Description=$dtcloud_dir
Requires=postgresql.service
After=network.target postgresql.service
[Service]
Type=simple
SyslogIdentifier=$dtcloud_dir
PermissionsStartOnly=true
User=root
Group=root
ExecStart=/opt/dtcloud/$dtcloud_dir/env/bin/python3 /opt/dtcloud/$dtcloud_dir/dtcloud.py -c /opt/dtcloud/$dtcloud_dir/dtcloud.conf
StandardOutput=journal+console
[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl stop $dtcloud_dir
sudo systemctl start $dtcloud_dir

sleep 20s

# 6.0 结束
echo "安装结束,请访问以下地址进入DTCloud:http://$server_ip:$xmlrpc_port" 



